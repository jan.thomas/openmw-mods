# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from pybuild import Pybuild1, InstallDir, File, apply_patch


class Package(Pybuild1):
    NAME = "Bitter Coast Sounds"
    DESC = "Adds ambient noise and dragonflies all over the Bitter Coast region"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    DEPEND = """
    sys-bin/convert
    >=bin/delta-plugin-0.12 <bin/delta-plugin-0.15
    """
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/bittercoastsounds.zip
        https://gitlab.com/bmwinger/umopp/uploads/4af9afb057b5dcbf31c43d5aead72c5c/bcsounds-umopp-3.1.0.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("bcsounds.esp")], S="bittercoastsounds"),
        InstallDir(".", S="bcsounds-umopp-3.1.0"),
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "bcsounds-umopp-3.1.0")
        self.execute("delta_plugin -v apply " + os.path.join(path, "bcsounds.patch"))
        self.execute("magick convert Textures/Photodragon.tga Textures/Photodragon.dds")
        os.remove("Textures/Photodragon.tga")
        apply_patch(os.path.join(path, "Photodragon_mesh.patch"))
